import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NoteService } from '../note-service/note.service';
import { FormGroup, FormControl } from '@angular/forms';
import { title } from 'process';

@Component({
  selector: 'app-update-note',
  templateUrl: './update-note.component.html',
  styleUrls: ['./update-note.component.css'],
})
export class UpdateNoteComponent implements OnInit {
  noteForm = new FormGroup({
    noteUuid: new FormControl(''),
    title: new FormControl(''),
    message: new FormControl(''),
  });


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private noteService: NoteService,
  ) { }

  ngOnInit() {
    this.noteForm.controls.noteUuid.setValue(
      this.activatedRoute.snapshot.params.noteUuid
    );
  }

  propose() {
    console.log(this.noteForm);
    this.noteService.propose(
       this.noteForm.value.noteUuid,
       this.noteForm.value.title,
       this.noteForm.value.message
    ).subscribe({
       next: data => {
         console.log(data)
         this.router.navigateByUrl('/list/note');
        //  this.noteForm.value.noteUuid.setValue('');
        //  this.noteForm.controls.title.setValue('');
        //  this.noteForm.controls.message.setValue('');
       },
       error: err => {
         console.log({ err })
       },
     });
  }


}
