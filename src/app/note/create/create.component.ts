import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { NoteService } from '../note-service/note.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {
  noteForm = new FormGroup({
    title: new FormControl(''),
    message: new FormControl(''),
  });

  constructor(
    private http: HttpClient,
    private noteService: NoteService,
    private router: Router,
  ) {}

  ngOnInit() {}

  create() {
    this.noteService
      .create(
        this.noteForm.controls.title.value,
        this.noteForm.controls.message.value,
      )
      .subscribe({
        next: data => {
          this.noteForm.controls.title.setValue('');
          this.noteForm.controls.message.setValue('');
          this.router.navigateByUrl('/list/note');
        },
      });
  }
}
