import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router, ActivatedRoute } from '@angular/router';
import { NoteService } from 'src/app/note/note-service/note.service';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  uuid;
  titleNew;
  messageNew;
  titleOld;
  messageOld;
  constructor(
    private oauthService: OAuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private noteService: NoteService,
    private snackBar:MatSnackBar,
  ) {}

  ngOnInit() {
    this.uuid = this.activatedRoute.snapshot.params.uuid
    this.noteService.retrieve(this.uuid).subscribe({
      next:(response:any) =>{
        console.log(response)
        if(response.note.isProposed === false){
          this.snackBar.open("Note-Edit is not proposed",'Close',{duration:2500})
          this.router.navigateByUrl('/update-note/' + this.uuid);
        }

        this.titleOld = response.note.title
        this.messageOld = response.note.message;
        this.titleNew = response.editNote[0].title
        this.messageNew = response.editNote[0].message
      },
      error:err =>{console.log(err)}
    })
  }

  approve(uuid){
    this.noteService.approve(this.uuid).subscribe({
      next: data =>
      {
        console.log(data)
        this.router.navigateByUrl('/list/note')

      },
      error :err =>
      {
        console.log(err)
      }
    })
  }

  dissapprove(){
    this.noteService.dissapprove(this.uuid).subscribe({
      next :data =>
      {
        console.log(data)
      },
      error : err =>
      {
        console.log(err)
      }
    })
  }


}